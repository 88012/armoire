## Armoire

## Design

We used Django for the back-end and React for the front-end. We utilized RESTful APIs to get a list of shoes/hats, create new shoes/hats, and delete shoes/hats. The urls route to our views which is similar to the middle ground between urls and models.

## Shoes microservice

The models for the Shoes microservice are Shoe and BinVO. We used poller to pull Wardrobe API data to our microservice.

## Hats microservice

Location has closet name, section number, and shelf number. Hat model has properties "fabric", "style", "color", "picture", "location". Encoders were used for views
