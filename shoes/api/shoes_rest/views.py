from django.shortcuts import render
from .models import BinVO, Shoe
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder = ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            bin_href = content["bin"]
            print(bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status = 400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder = ShoesListEncoder,
            safe = False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        try:
            shoes = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoes,
                encoder = ShoeDetailEncoder,
                safe = False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe"},
                status = 400,
            )
    elif request.method == "DELETE":
        try:
            shoes = Shoe.objects.get(id=pk)
            shoes.delete()
            return JsonResponse(
                shoes,
                encoder = ShoeDetailEncoder,
                safe = False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe"},
                status = 400,
            )
    else: # PUT
        try:
            content = json.loads(request.body)
            shoes = Shoe.objects.get(id=pk)

            props = ["manufacturer", "model_name", "color", "picture_url", "id"]
            for prop in props:
                if prop in content:
                    setattr(shoes, prop, content[prop])
            shoes.save()
            return JsonResponse(
                shoes,
                encoder = ShoesListEncoder,
                safe = False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid shoe"},
                status = 400
            )
