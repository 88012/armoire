import React from "react";

class ShoesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            shoes: []
        };
    }

    async componentDidMount() {
        const url = ('http://localhost:8080/api/shoes/');
        const response = await fetch(url);
        if (response.ok) {
            const data =await response.json();
            this.setState({shoes:data.shoes});
        }
    }

    async handleDelete(id) {
        const shoesUrl = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const shoeResponse = await fetch(shoesUrl, fetchConfig);
        if (shoeResponse.ok) {
            this.setState ({shoes: this.state.shoes.filter(shoe => shoe.id != id)})
        }
        else {
            throw new Error ("Error");
        }
    }

    render() {
        return (
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>Manufacturer</th>
                            <th>Model Name</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Bin</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.shoes.map(shoe => {
                            return (
                                <tr key={shoe.id}>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.picture_url}</td>
                                <td><img src={shoe.picture} className = 'pic-thumbnail' width="120px" height = "120px" /></td>
                                <td><button className="btn btn-danger" onClick={() => this.handleDelete(shoe.id)}>Delete</button></td>
                                </tr>
                            );
                            })}
                    </tbody>
                </table>
            </div>
        );
                        };
                    }

export default ShoesList;
