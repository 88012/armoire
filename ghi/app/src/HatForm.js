import React from 'react';

class HatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fabric: '',
            style:'',
            color:'',
            picture: '',
            locations: [],
            };

        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          this.setState({ locations: data.locations });
        }
      }

      handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
      }

      handleStyleChange(event) {
        const value = event.target.value;
        this.setState({ style: value });
      }

      handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
      }

      handlePictureChange(event) {
        const value = event.target.value;
        this.setState({ picture: value });
      }

      handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value });
      }

      async handleSubmit(event) {
        event.preventDefault();
        let data = {...this.state};
        delete data.locations;

        const locationUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          this.setState({
            fabric: '',
            style: '',
            color: '',
            picture:'',
            location: '',
          });
        }
      }


      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a new hat</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleFabricChange} value={this.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="fabric_name">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStyleChange} value={this.style} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                    <label htmlFor="style_email">Style</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleColorChange} value={this.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color_name">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePictureChange} value={this.picture} placeholder="Picture" required type="text" name="picture" id="picture" className="form-control" />
                    <label htmlFor="picture_name">Picture Link</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} value={this.location}  placeholder="Location" required name="location" className="text" id="location">
                      <option value="">Choose a location</option>
                      {this.state.locations.map(location => {
                        return (
                          <option key={location.id} value={location.href}>{location.closet_name}</option>
                        )
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }

    export default HatForm;
