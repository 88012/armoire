function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center text-black">
      <h1 className="display-5 fw-bold">Armoire</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Shoes and Hats Organization
        </p>
      </div>
    </div>
  );
}

export default MainPage;
