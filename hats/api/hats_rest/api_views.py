from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style", "fabric", "color", "id", "picture"]

    def get_extra_data(self,o):
        return {"location": o.location.closet_name, "location_href": o.location.import_href}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style", "color", "picture", "location", "id"]
    encoders = {"location": LocationVODetailEncoder(),}

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(import_href=content["location"])
            print(location)
            content["location"]=location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Location id"}, status=400,)

        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False,)

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
        except Hat.DoesNotExist:
            response=JsonResponse({"message": "Sorry, this does not exist"})
            response.status_code=404
            return response

    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count>0})

    else:
        content = json.loads(request.body)
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat, encoder=HatDetailEncoder, safe=False
        )
# Create your views here.
